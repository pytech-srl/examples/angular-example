import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
})
export class TodosComponent implements OnInit {
  constructor() {
  }

  ngOnInit(): void {
  }


  todos = [
    {
      id: 1,
      title: 'Studiare Angular',
      completed: false,
      isEditable: false,
    },
    {
      id: 2,
      title: 'Fare la spesa',
      completed: false,
      isEditable: false,
    },
    {
      id: 3,
      title: 'Portare fuori il cane',
      completed: false,
      isEditable: false,
    },
    {
      id: 4,
      title: 'Pagare le bollette',
      completed: false,
      isEditable: false,
    },
  ];

  isCompleted(todo: any) {
    todo.completed = !todo.completed
  };

  setClass(todo: any) {
    return {
      completed: todo.completed
    };
  }

  deleteItem(todo: any) {
    this.todos = this.todos.filter(item => item !== todo);
  }

  toggleEditMode(todo: any) {
    todo.isEditable = !todo.isEditable;
  }

  updateTodo(event: any, todo: any) {
    todo.title = event.target.value;
    todo.isEditable = !todo.isEditable;
  }
}
